package main

import (
	"net"
	"time"
)

type Player struct {
	conn         net.Conn
	isDead       bool
	ID           int
	Name         string
	VersionMajor int
	VersionMinor int
	MovesLeft    int
	FiresLeft    int
	X            int
	Y            int
	Health       int
	GamesWon     int
}

const rwTimeout int = 2

func (p *Player) Write(b []byte) bool {
	p.conn.SetWriteDeadline(time.Now().Add(time.Duration(rwTimeout) * time.Second))
	_, err := p.conn.Write(b)
	if err != nil {
		return false
	}
	return true
}

func (p *Player) Read() []byte {
	b := make([]byte, 32)

	p.conn.SetReadDeadline(time.Now().Add(time.Duration(rwTimeout) * time.Second))
	n, err := p.conn.Read(b)
	if err != nil {
		return nil
	}

	return b[:n]
}
