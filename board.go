package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"sort"
	"time"
)

// Direction consts
const (
	NorthWest int = iota
	North
	NorthEast
	East
	SouthEast
	South
	SouthWest
	West
)

type Board struct {
	cells     [][]int // 0 = EMPTY, 1 = BLOCK, [2-255] = IDs
	width     int
	height    int
	lastID    int
	log_buf   bytes.Buffer
	logs      *log.Logger
	logName   string
	printLogs bool
	Demo      *Replay
	Keys      []int
	Players   map[int]*Player
}

func dirStr(d int) string {
	str := []string{
		"northwest",
		"north",
		"northeast",
		"east",
		"southeast",
		"south",
		"southwest",
		"west",
	}

	return str[d]
}

func NewBoard(logs bool, width, height int) *Board {
	var b Board

	b.cells = make([][]int, width)
	for i := range b.cells {
		b.cells[i] = make([]int, height)
	}

	b.width = width
	b.height = height
	b.lastID = 1
	b.logs = log.New(&b.log_buf, "[SERVER] ", log.Lmicroseconds)
	b.Players = make(map[int]*Player)
	b.printLogs = logs
	b.logName = time.Now().Format("20060102-15-04-05")
	b.Demo = NewReplay(b.logName + ".kia")

	return &b
}

func (b *Board) ResetBoard() {
	for _, v := range b.Players {
		v.isDead = false
		v.Health = maxHealth
		b.cells[v.X][v.Y] = 0
		for {
			x := int(rand.Uint32() % uint32(b.width))
			y := int(rand.Uint32() % uint32(b.height))

			if b.cells[x][y] != 0 {
				continue
			} else {
				b.cells[x][y] = v.ID
				v.X, v.Y = x, y
				break
			}
		}
	}
}

func (b *Board) PlayersLeft() int {
	i := 0
	for _, v := range b.Players {
		if !v.isDead {
			i += 1
		}
	}
	return i
}

func (b *Board) PlayersConnected() int {
	return len(b.Players)
}

func (b *Board) AddPlayer(conn net.Conn, name string, maj, min, move, fire, health int) int {
	var x, y int

	b.lastID += 1

	for {
		x = int(rand.Uint32() % uint32(b.width))
		y = int(rand.Uint32() % uint32(b.height))

		if b.cells[x][y] != 0 {
			continue
		} else {
			b.cells[x][y] = b.lastID
			break
		}
	}

	p := &Player{
		conn,
		false,
		b.lastID,
		name,
		int(maj),
		int(min),
		move,
		fire,
		x,
		y,
		health,
		0,
	}

	b.Players[p.ID] = p

	// Add sorting key
	b.Keys = append(b.Keys, p.ID)

	b.Demo.CmdAddPlayer(name, p.ID, x, y, int(maj), int(min))

	return p.ID
}

func (b *Board) RemovePlayer(id int) {
	b.Players[id].conn.Close()
	b.cells[b.Players[id].X][b.Players[id].Y] = 0
	delete(b.Players, id)

	// Remove sorting key
	for i, v := range b.Keys {
		if v == id {
			b.Keys = append(b.Keys[:i], b.Keys[i+1:]...)
			break
		}
	}

	b.Demo.CmdRemovePlayer(id)
}

func (b *Board) SortPlayers() {
	sort.Ints(b.Keys)
}

func (b *Board) Move(id, d int) {
	if b.Players[id].MovesLeft > 0 {
		var x, y int

		px := b.Players[id].X
		py := b.Players[id].Y

		switch d {
		case NorthWest:
			x = px - 1
			y = py - 1

		case North:
			x = px
			y = py - 1

		case NorthEast:
			x = px + 1
			y = py - 1

		case East:
			x = px + 1
			y = py

		case SouthEast:
			x = px + 1
			y = py + 1

		case South:
			x = px
			y = py + 1

		case SouthWest:
			x = px - 1
			y = py + 1

		case West:
			x = px - 1
			y = py
		}

		// If the new position is inside the board and empty then move player
		if x < b.width && x >= 0 && y < b.height && y >= 0 {
			if b.cells[x][y] == 0 {
				// Move player
				b.cells[px][py] = 0
				b.cells[x][y] = id
				b.Players[id].X, b.Players[id].Y = x, y

				// Reduce movesLeft
				b.Players[id].MovesLeft--

				b.Log("Moved from (%v, %v) to (%v, %v).\n", px, py, x, y)
				b.Demo.CmdMove(id, d)
			} else {
				b.Log("Can't move because there is another player in %v.\n", dirStr(d))
			}
		} else {
			b.Log("Can't move because %v is blocked.\n", dirStr(d))
		}
	} else {
		b.Log("Can't move bacause there is no moves left.\n")
	}
}

func (b *Board) Fire(id, d int) {
	if b.Players[id].FiresLeft > 0 {
		px := b.Players[id].X
		py := b.Players[id].Y

		b.Demo.CmdFire(id, d)

		switch d {
		case NorthWest:
			for px, py = px-1, py-1; px >= 0 && py >= 0; px, py = px-1, py-1 {
				enemyID := b.cells[px][py]
				if v, ok := b.Players[enemyID]; ok {
					v.Health--
					b.Demo.CmdDamagePlayer(enemyID)
				}
			}

		case North:
			for py = py - 1; py >= 0; py -= 1 {
				enemyID := b.cells[px][py]
				if v, ok := b.Players[enemyID]; ok {
					v.Health--
					b.Demo.CmdDamagePlayer(enemyID)
				}
			}

		case NorthEast:
			for px, py = px+1, py-1; px < b.width && py >= 0; px, py = px+1, py-1 {
				enemyID := b.cells[px][py]
				if v, ok := b.Players[enemyID]; ok {
					v.Health--
					b.Demo.CmdDamagePlayer(enemyID)
				}
			}

		case East:
			for px = px + 1; px < b.width; px += 1 {
				enemyID := b.cells[px][py]
				if v, ok := b.Players[enemyID]; ok {
					v.Health--
					b.Demo.CmdDamagePlayer(enemyID)
				}
			}

		case SouthEast:
			for px, py = px+1, py+1; px < b.width && py < b.height; px, py = px+1, py+1 {
				enemyID := b.cells[px][py]
				if v, ok := b.Players[enemyID]; ok {
					v.Health--
					b.Demo.CmdDamagePlayer(enemyID)
				}
			}

		case South:
			for py = py + 1; py < b.height; py += 1 {
				enemyID := b.cells[px][py]
				if v, ok := b.Players[enemyID]; ok {
					v.Health--
					b.Demo.CmdDamagePlayer(enemyID)
				}
			}

		case SouthWest:
			for px, py = px-1, py+1; px >= 0 && py < b.height; px, py = px-1, py+1 {
				enemyID := b.cells[px][py]
				if v, ok := b.Players[enemyID]; ok {
					v.Health--
					b.Demo.CmdDamagePlayer(enemyID)
				}
			}

		case West:
			for px = px - 1; px >= 0; px -= 1 {
				enemyID := b.cells[px][py]
				if v, ok := b.Players[enemyID]; ok {
					v.Health--
					b.Demo.CmdDamagePlayer(enemyID)
				}
			}
		}

		b.Players[id].FiresLeft--
		b.Log("Fired successfully in direction %v.\n", dirStr(d))

		// Remove dead players
		for pid, p := range b.Players {
			if p.Health == 0 && p.isDead == false {
				b.Log("Player '%v' with ID %v died.\n", p.Name, pid)
				b.cells[p.X][p.Y] = 0
				p.isDead = true
				b.Demo.CmdKillPlayer(pid)
			}
		}
	} else {
		b.Log("Can't fire because there is no fires left.\n")
	}
}

func (b *Board) GetRadar(id int) []int {
	px := b.Players[id].X
	py := b.Players[id].Y

	radar := make([]int, 8)

	if px != 0 && py != 0 && px != b.width-1 && py != b.height-1 {
		radar[NorthWest] = b.cells[px-1][py-1]
		radar[North] = b.cells[px][py-1]
		radar[NorthEast] = b.cells[px+1][py-1]
		radar[East] = b.cells[px+1][py]
		radar[SouthEast] = b.cells[px+1][py+1]
		radar[South] = b.cells[px][py+1]
		radar[SouthWest] = b.cells[px-1][py+1]
		radar[West] = b.cells[px-1][py]
	} else if px == 0 && py == 0 {
		radar[NorthWest] = 1
		radar[North] = 1
		radar[NorthEast] = 1
		radar[East] = b.cells[px+1][py]
		radar[SouthEast] = b.cells[px+1][py+1]
		radar[South] = b.cells[px][py+1]
		radar[SouthWest] = 1
		radar[West] = 1
	} else if px == 0 && py == b.height-1 {
		radar[NorthWest] = 1
		radar[North] = b.cells[px][py-1]
		radar[NorthEast] = b.cells[px+1][py-1]
		radar[East] = b.cells[px+1][py]
		radar[SouthEast] = 1
		radar[South] = 1
		radar[SouthWest] = 1
		radar[West] = 1
	} else if px == b.width-1 && py == 0 {
		radar[NorthWest] = 1
		radar[North] = 1
		radar[NorthEast] = 1
		radar[East] = 1
		radar[SouthEast] = 1
		radar[South] = b.cells[px][py+1]
		radar[SouthWest] = b.cells[px-1][py+1]
		radar[West] = b.cells[px-1][py]
	} else if px == b.width-1 && py == b.height-1 {
		radar[NorthWest] = b.cells[px-1][py-1]
		radar[North] = b.cells[px][py-1]
		radar[NorthEast] = 1
		radar[East] = 1
		radar[SouthEast] = 1
		radar[South] = 1
		radar[SouthWest] = 1
		radar[West] = b.cells[px-1][py]
	} else if px > 0 && px < b.width-1 && py == 0 {
		radar[NorthWest] = 1
		radar[North] = 1
		radar[NorthEast] = 1
		radar[East] = b.cells[px+1][py]
		radar[SouthEast] = b.cells[px+1][py+1]
		radar[South] = b.cells[px][py+1]
		radar[SouthWest] = b.cells[px-1][py+1]
		radar[West] = b.cells[px-1][py]
	} else if px > 0 && px < b.width-1 && py == b.height-1 {
		radar[NorthWest] = b.cells[px-1][py-1]
		radar[North] = b.cells[px][py-1]
		radar[NorthEast] = b.cells[px+1][py-1]
		radar[East] = b.cells[px+1][py]
		radar[SouthEast] = 1
		radar[South] = 1
		radar[SouthWest] = 1
		radar[West] = b.cells[px-1][py]
	} else if px == b.width-1 && py > 0 && py < b.height-1 {
		radar[NorthWest] = b.cells[px-1][py-1]
		radar[North] = b.cells[px][py-1]
		radar[NorthEast] = 1
		radar[East] = 1
		radar[SouthEast] = 1
		radar[South] = b.cells[px][py+1]
		radar[SouthWest] = b.cells[px-1][py+1]
		radar[West] = b.cells[px-1][py]
	} else { // else if px == 0 && py > 0 && py < b.height-1
		radar[NorthWest] = 1
		radar[North] = b.cells[px][py-1]
		radar[NorthEast] = b.cells[px+1][py-1]
		radar[East] = b.cells[px+1][py]
		radar[SouthEast] = b.cells[px+1][py+1]
		radar[South] = b.cells[px][py+1]
		radar[SouthWest] = 1
		radar[West] = 1
	}

	return radar
}

func (b *Board) Log(str string, v ...interface{}) {
	b.logs.Printf(str, v...)
	if b.printLogs {
		fmt.Printf(str, v...)
	}
}

func (b *Board) SaveLog() {
	// Print tournament stats as the last thing
	if tournament > 1 {
		for _, v := range b.Players {
			b.Log("Player '%v' with ID %v won %v games.\n", v.Name, v.ID, v.GamesWon)
		}
		b.Log("\n")
	}

	b.Log("Game ended.\n\n")

	ioutil.WriteFile(b.logName+".log", b.log_buf.Bytes(), 0777)
}
