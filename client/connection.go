package client

import (
	"fmt"
	"net"
	"os"
)

type connection struct {
	conn net.Conn
}

func (c connection) send(b []byte) {
	_, err := c.conn.Write(b)
	if err != nil {
		fmt.Printf("[CLIENT] Connection to %v is closed.\n", c.conn.RemoteAddr())
		fmt.Printf("[CLIENT] Quitting...\n")
		os.Exit(1)
	}
}

func (c connection) receive() []byte {
	b := make([]byte, 32)

	n, err := c.conn.Read(b)
	if err != nil {
		fmt.Printf("[CLIENT] Connection to %v is closed.\n", c.conn.RemoteAddr())
		fmt.Printf("[CLIENT] Quitting...\n")
		os.Exit(1)
	}

	return b[:n]
}

// Global connection
var conn connection
