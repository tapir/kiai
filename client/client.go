package client

import (
	"fmt"
	"net"
	"os"
)

// Direction consts
const (
	NorthWest int = iota
	North
	NorthEast
	East
	SouthEast
	South
	SouthWest
	West
)

// Game config is recieved from server when connection is established.
// Holds data about game preferences.
type GameConfig struct {
	Width       int
	Height      int
	MaxMoves    int
	MaxFires    int
	MaxHealth   int
	PlayerCount int
	Timeout     int
}

// Holds position, health, players left in the game and radar data.
type TurnStatus struct {
	X           int
	Y           int
	Health      int
	MovesLeft   int
	FiresLeft   int
	PlayersLeft int
	Radar       []int
}

// Connect to server send player data and receive game config.
func Connect(server, playerName string, playerVersionMajor, playerVersionMinor int) GameConfig {
	// Establish connection to server
	var err error
	conn.conn, err = net.Dial("tcp", server)
	if err != nil {
		fmt.Printf("[CLIENT] Can't connect to remote host %v\n", server)
		fmt.Printf("[CLIENT] Quitting...\n")
		os.Exit(1)
	}

	// Send player data to server
	playerData := encodePlayerData(playerName, playerVersionMajor, playerVersionMinor)
	conn.send(playerData)

	// Receive game config from server
	buffer := conn.receive()
	width, height, maxMoves, maxFires, maxHealth, playerCount, timeout := decodeGameConfig(buffer)

	// Connection success
	fmt.Printf("[CLIENT] Connection to %v is successfull.\n", server)

	// Return data
	return GameConfig{width, height, maxMoves, maxFires, maxHealth, playerCount, timeout}
}

// Disconnect from server
func Disconnect() {
	conn.conn.Close()
	fmt.Println("[CLIENT] Connection closed.")
}

// Move player 1 step in given direction and receive new move status.
func (s *TurnStatus) Move(d int) {
	// Send
	m := encodeMove(d)
	conn.send(m)

	// Receive
	buffer := conn.receive()

	x, y, movesLeft, radar := decodeMove(buffer)

	s.X = x
	s.Y = y
	s.MovesLeft = movesLeft
	copy(s.Radar, radar)
}

// Fire 1 shot in given direction and receive fire status.
func (s *TurnStatus) Fire(d int) {
	// Send
	f := encodeFire(d)
	conn.send(f)

	// Receive
	buffer := conn.receive()

	firesLeft, playersLeft, radar := decodeFire(buffer)

	s.FiresLeft = firesLeft
	s.PlayersLeft = playersLeft
	copy(s.Radar, radar)
}

// Wait for player's turn and receive turn status.
func WaitForTurn() *TurnStatus {
	// Receive
	buffer := conn.receive()

	x, y, health, movesLeft, firesLeft, playersLeft, radar := decodeNewTurn(buffer)

	return &TurnStatus{x, y, health, movesLeft, firesLeft, playersLeft, radar}
}

// End turn
func EndTurn() {
	conn.send(encodeEndTurn())
}
