package client

import (
	"bytes"
	"encoding/binary"
)

const (
	playerDataID byte = iota + 1
	_
	moveID
	fireID
	endTurnID
)

func encodePlayerData(playerName string, playerVersionMajor int, playerVersionMinor int) []byte {
	// 1  byte for message type
	// 16 bytes for robotName
	// 1  byte for robotVersionMajor
	// 1  byte for robotVersionMinor

	buffer := new(bytes.Buffer)

	name := make([]byte, 16)
	copy(name, []byte(playerName))
	name[15] = 0

	var data = []interface{}{
		playerDataID,
		name,
		uint8(playerVersionMajor),
		uint8(playerVersionMinor),
	}

	for _, v := range data {
		binary.Write(buffer, binary.BigEndian, v)
	}

	return buffer.Bytes()
}

func encodeMove(d int) []byte {
	// 1 byte for message type
	// 1 byte for direction

	buffer := new(bytes.Buffer)

	var data = []interface{}{
		moveID,
		uint8(d),
	}

	for _, v := range data {
		binary.Write(buffer, binary.BigEndian, v)
	}

	return buffer.Bytes()
}

func encodeFire(d int) []byte {
	// 1 byte for message type
	// 1 byte for direction

	buffer := new(bytes.Buffer)

	var data = []interface{}{
		fireID,
		uint8(d),
	}

	for _, v := range data {
		binary.Write(buffer, binary.BigEndian, v)
	}

	return buffer.Bytes()
}

func encodeEndTurn() []byte {
	// 1 byte for message type

	buffer := new(bytes.Buffer)

	var data = []interface{}{
		endTurnID,
	}

	for _, v := range data {
		binary.Write(buffer, binary.BigEndian, v)
	}

	return buffer.Bytes()
}

func decodeNewTurn(b []byte) (int, int, int, int, int, int, []int) {
	type data struct {
		_           uint8
		X           uint8
		Y           uint8
		Health      uint8
		MovesLeft   uint8
		FiresLeft   uint8
		PlayersLeft uint8
		Radar       [8]uint8
	}

	var d data
	binary.Read(bytes.NewReader(b), binary.BigEndian, &d)

	radarData := make([]int, 8)
	for i, v := range d.Radar {
		radarData[i] = int(v)
	}

	return int(d.X), int(d.Y), int(d.Health), int(d.MovesLeft), int(d.FiresLeft), int(d.PlayersLeft), radarData[:]
}

func decodeMove(b []byte) (int, int, int, []int) {
	type data struct {
		_         uint8
		X         uint8
		Y         uint8
		MovesLeft uint8
		Radar     [8]uint8
	}

	var d data
	binary.Read(bytes.NewReader(b), binary.BigEndian, &d)

	radarData := make([]int, 8)
	for i, v := range d.Radar {
		radarData[i] = int(v)
	}

	return int(d.X), int(d.Y), int(d.MovesLeft), radarData[:]
}

func decodeFire(b []byte) (int, int, []int) {
	type data struct {
		_           uint8
		PlayersLeft uint8
		FiresLeft   uint8
		Radar       [8]uint8
	}

	var d data
	binary.Read(bytes.NewReader(b), binary.BigEndian, &d)

	radarData := make([]int, 8)
	for i, v := range d.Radar {
		radarData[i] = int(v)
	}

	return int(d.FiresLeft), int(d.PlayersLeft), radarData[:]
}

func decodeGameConfig(b []byte) (int, int, int, int, int, int, int) {
	type data struct {
		_           uint8
		Width       uint8
		Height      uint8
		MaxMoves    uint8
		MaxFires    uint8
		MaxHealth   uint8
		PlayerCount uint8
		Timeout     uint8
	}

	var d data
	binary.Read(bytes.NewReader(b), binary.BigEndian, &d)

	return int(d.Width), int(d.Height), int(d.MaxMoves), int(d.MaxFires), int(d.MaxHealth), int(d.PlayerCount), int(d.Timeout)
}
