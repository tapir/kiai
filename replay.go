package main

import (
	"encoding/json"
	"io/ioutil"
)

type fileVersion struct {
	Command string
	Version string
}

type newServer struct {
	Command     string
	BoardWidth  int
	BoardHeight int
	MaxMoves    int
	MaxFires    int
	MaxHealth   int
}

type addPlayer struct {
	Command string
	Name    string
	ID      int
	X       int
	Y       int
	Major   int
	Minor   int
}

type resetPlayer struct {
	Command string
	ID      int
	X       int
	Y       int
}

type removePlayer struct {
	Command string
	ID      int
}

type newGame struct {
	Command string
	Count   int
}

type newTurn struct {
	Command string
	Count   int
	ID      int
}

type move struct {
	Command   string
	ID        int
	Direction int
}

type fire struct {
	Command   string
	ID        int
	Direction int
}

type damagePlayer struct {
	Command string
	ID      int
}

type killPlayer struct {
	Command string
	ID      int
}

type winner struct {
	Command string
	Name    string
	ID      int
}

type Replay struct {
	fileName string
	buffer   []interface{}
}

func NewReplay(fileName string) *Replay {
	r := new(Replay)
	r.fileName = fileName
	r.buffer = append(r.buffer, fileVersion{"FILE_VERSION", "KIA_JSON_0.9"})
	return r
}

func (r *Replay) Save() {
	b, _ := json.Marshal(r.buffer)
	ioutil.WriteFile(r.fileName, b, 0777)
}

func (r *Replay) CmdNewServer(bw, bh, mm, mf, mh int) {
	r.buffer = append(r.buffer, newServer{"NEW_SERVER", bw, bh, mm, mf, mh})
}

func (r *Replay) CmdAddPlayer(name string, id, x, y, maj, min int) {
	r.buffer = append(r.buffer, addPlayer{"ADD_PLAYER", name, id, x, y, maj, min})
}

func (r *Replay) CmdResetPlayer(id, x, y int) {
	r.buffer = append(r.buffer, resetPlayer{"RESET_PLAYER", id, x, y})
}

func (r *Replay) CmdRemovePlayer(id int) {
	r.buffer = append(r.buffer, removePlayer{"REMOVE_PLAYER", id})
}

func (r *Replay) CmdNewGame(count int) {
	r.buffer = append(r.buffer, newGame{"NEW_GAME", count})
}

func (r *Replay) CmdNewTurn(count, id int) {
	r.buffer = append(r.buffer, newTurn{"NEW_TURN", count, id})
}

func (r *Replay) CmdMove(id, dir int) {
	r.buffer = append(r.buffer, move{"MOVE", id, dir})
}

func (r *Replay) CmdFire(id, dir int) {
	r.buffer = append(r.buffer, fire{"FIRE", id, dir})
}

func (r *Replay) CmdDamagePlayer(id int) {
	r.buffer = append(r.buffer, damagePlayer{"DAMAGE_PLAYER", id})
}

func (r *Replay) CmdKillPlayer(id int) {
	r.buffer = append(r.buffer, killPlayer{"KILL_PLAYER", id})
}

func (r *Replay) CmdWinner(name string, id int) {
	r.buffer = append(r.buffer, winner{"WINNER", name, id})
}
