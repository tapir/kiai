package main

import (
	"bytes"
	"encoding/binary"
)

const (
	PlayerDataID uint8 = iota + 1
	GameConfigID
	MoveID
	FireID
	EndTurnID
	NewTurnID
)

func EncodeGameConfig(width, height, maxMoves, maxFires, health, playerCount, timeout int) []byte {
	// 1 byte for message type
	// 1 byte for board width
	// 1 byte for board height
	// 1 byte for max moves allowed per turn
	// 1 byte for max fires alowed per turn
	// 1 byte for max health
	// 1 byte for number of players
	// 1 byte for turn timeout

	buffer := new(bytes.Buffer)

	var data = []interface{}{
		GameConfigID,
		uint8(width),
		uint8(height),
		uint8(maxMoves),
		uint8(maxFires),
		uint8(health),
		uint8(playerCount),
		uint8(timeout),
	}

	for _, v := range data {
		binary.Write(buffer, binary.BigEndian, v)
	}

	return buffer.Bytes()
}

func EncodeNewTurn(x, y, health, movesLeft, firesLeft, playersLeft int, radarData []int) []byte {
	// 1 byte for message type
	// 2 byte for position
	// 1 byte for health
	// 1 byte for moves left
	// 1 byte for fires left
	// 1 byte for players left
	// 8 byte for radar data

	buffer := new(bytes.Buffer)

	radar := make([]uint8, 8)
	for i, v := range radarData {
		radar[i] = uint8(v)
	}

	var data = []interface{}{
		NewTurnID,
		uint8(x),
		uint8(y),
		uint8(health),
		uint8(movesLeft),
		uint8(firesLeft),
		uint8(playersLeft),
		radar,
	}

	for _, v := range data {
		binary.Write(buffer, binary.BigEndian, v)
	}

	return buffer.Bytes()
}

func EncodeMove(x, y, movesLeft int, radarData []int) []byte {
	// 1 byte for message type
	// 2 bytes for position
	// 1 byte for moves left
	// 8 bytes for radar data

	buffer := new(bytes.Buffer)

	radar := make([]uint8, 8)
	for i, v := range radarData {
		radar[i] = uint8(v)
	}

	var data = []interface{}{
		MoveID,
		uint8(x),
		uint8(y),
		uint8(movesLeft),
		radar,
	}

	for _, v := range data {
		binary.Write(buffer, binary.BigEndian, v)
	}

	return buffer.Bytes()
}

func EncodeFire(playersLeft, firesLeft int, radarData []int) []byte {
	// 1 byte for message type
	// 1 byte for board players left
	// 1 byte for board fires left
	// 8 bytes for radar data

	buffer := new(bytes.Buffer)

	radar := make([]uint8, 8)
	for i, v := range radarData {
		radar[i] = uint8(v)
	}

	var data = []interface{}{
		FireID,
		uint8(playersLeft),
		uint8(firesLeft),
		radar,
	}

	for _, v := range data {
		binary.Write(buffer, binary.BigEndian, v)
	}

	return buffer.Bytes()
}

func DecodePlayerData(b []byte) (string, int, int, bool) {
	// 1  byte for message type
	// 16 bytes for playerName
	// 1  byte for robotVersionMajor
	// 1  byte for robotVersionMinor

	if len(b) != 19 || b[0] != PlayerDataID {
		return "", 0, 0, true
	}

	type data struct {
		_                  uint8
		PlayerName         [16]byte
		PlayerVersionMajor uint8
		PlayerVersionMinor uint8
	}

	var d data
	binary.Read(bytes.NewReader(b), binary.BigEndian, &d)

	// Clean name
	i := 15
	for ; i >= 0; i-- {
		if d.PlayerName[i] != 0 {
			break
		}
	}
	rstr := d.PlayerName[:i+1]
	if i == 0 {
		rstr = []byte{0}
	}

	// Check if player name is legal
	for _, v := range rstr {
		if int(v) < 33 || int(v) > 126 {
			return "", 0, 0, true
		}
	}

	return string(rstr), int(d.PlayerVersionMajor), int(d.PlayerVersionMinor), false
}

func DecodeMove(b []byte) (int, bool) {
	// 1  byte for message type
	// 1  byte for direction

	if len(b) != 2 {
		return 0, true
	}

	type data struct {
		_             uint8
		MoveDirection uint8
	}

	var d data
	binary.Read(bytes.NewReader(b), binary.BigEndian, &d)

	if !(d.MoveDirection >= 0 && d.MoveDirection <= 7) {
		return 0, true
	}

	return int(d.MoveDirection), false
}

func DecodeFire(b []byte) (int, bool) {
	// 1  byte for message type
	// 1  byte for direction

	if len(b) != 2 {
		return 0, true
	}

	type data struct {
		_             uint8
		FireDirection uint8
	}

	var d data
	binary.Read(bytes.NewReader(b), binary.BigEndian, &d)

	if !(d.FireDirection >= 0 && d.FireDirection <= 7) {
		return 0, true
	}

	return int(d.FireDirection), false
}
