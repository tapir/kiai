Sample Bot
----------

This bot moves to the the first empty cell and then looks for enemies. If it can find an enemy it attacks otherwise it attacks in a random direction.

```Go
package main

import (
	"fmt"
	kiai "gitlab.com/tapir/kiai/client"
	"math/rand"
	"time"
)

func main() {
	// Seed rand with unix time
	rand.Seed(time.Now().UnixNano())

	// BotTest 1.0
	c := kiai.Connect("localhost:4000", "DummyBot-Go", 2, 0)
	defer kiai.Disconnect()

	fmt.Printf("Board width: %v\n", c.Width)
	fmt.Printf("Board height: %v\n", c.Height)
	fmt.Printf("Maximum moves per turn: %v\n", c.MaxMoves)
	fmt.Printf("Maximum fires per turn: %v\n", c.MaxFires)
	fmt.Printf("Initial health: %v\n", c.MaxHealth)
	fmt.Printf("Number of bots: %v\n", c.PlayerCount)
	fmt.Printf("Turn timeout: %v\n", c.Timeout)

	for {
		t := kiai.WaitForTurn()
		fmt.Printf("\nNew turn.\n")

		// Fire if there is anybody around
		for i := 0; i < 8; i++ {
			if t.Radar[i] > 1 {
				for t.FiresLeft > 0 {
					t.Fire(i)
					fmt.Printf("Shots fired. Fires left: %v\n", t.FiresLeft)
				}
			}
		}

		// Move to random empty space and fire if there is anybody
		for t.MovesLeft > 0 {
			i := rand.Intn(8)
			if t.Radar[i] == 0 {
				t.Move(i)
				fmt.Printf("Moved. Moves left: %v\n", t.MovesLeft)
			} else if t.Radar[i] > 1 {
				for i := 0; i < 8; i++ {
					if t.Radar[i] > 1 {
						for t.FiresLeft > 0 {
							t.Fire(i)
							fmt.Printf("Shots fired. Fires left: %v\n", t.FiresLeft)
						}
					}
				}
			}
		}

		// Fire if any fires left
		for t.FiresLeft > 0 {
			i := rand.Intn(8)
			if t.Radar[i] != 1 {
				t.Fire(i)
				fmt.Printf("Shots fired. Fires left: %v\n", t.FiresLeft)
			}
		}

		// End turn
		kiai.EndTurn()
	}
}
```
