package main

import (
	"flag"
	"math/rand"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// Server configuration flags
var (
	boardWidth   int
	boardHeight  int
	maxMoves     int
	maxFires     int
	maxHealth    int
	tournament   int
	playerCount  int
	timeout      int = 3
	garbageLimit int = 10
)

func init() {
	flag.IntVar(&boardWidth, "W", 30, "Board width [10, 30]. Default 30.")
	flag.IntVar(&boardHeight, "H", 20, "Board height [10, 20]. Default 20.")
	flag.IntVar(&maxMoves, "m", 5, "Maximum moves allowed each turn [1, 255]. Default 5.")
	flag.IntVar(&maxFires, "f", 2, "Maximum fires allowed each turn [1, 255]. Default 2.")
	flag.IntVar(&maxHealth, "h", 3, "Initial health [1, 127]. Default 3.")
	flag.IntVar(&playerCount, "p", 2, "Number of players to compete [2, 5]. Default 2.")
	flag.IntVar(&tournament, "T", 1, "Amount of games to be played before deciding on a winner [1-1000]. Default 1.")
}

func main() {
	// Parse flags
	flag.Parse()

	// Config limits
	if boardWidth < 10 {
		boardWidth = 10
	} else if boardWidth > 30 {
		boardWidth = 30
	}
	if boardHeight < 10 {
		boardHeight = 10
	} else if boardHeight > 20 {
		boardHeight = 20
	}
	if maxMoves < 1 {
		maxMoves = 1
	} else if maxMoves > 255 {
		maxMoves = 255
	}
	if maxFires < 1 {
		maxFires = 1
	} else if maxFires > 255 {
		maxFires = 255
	}
	if maxHealth < 1 {
		maxHealth = 1
	} else if maxHealth > 127 {
		maxHealth = 127
	}
	if playerCount < 2 {
		playerCount = 2
	} else if playerCount > 5 {
		playerCount = 5
	}
	if tournament < 1 {
		tournament = 1
	} else if tournament > 1000 {
		tournament = 1000
	}

	// Seed rand
	rand.Seed(time.Now().UTC().UnixNano())

	// Create board
	level := NewBoard(true, boardWidth, boardHeight)

	// Handle Ctrl+C
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)
	go func() {
		<-c
		level.SaveLog()
		level.Demo.Save()
		os.Exit(1)
	}()

	// Create listener
	ln, err := net.Listen("tcp", ":4000")
	if err != nil {
		level.Log("Can't start listener: %v\n\n", err)
		os.Exit(1)
	}
	level.Log("Server started. Waiting for connections...\n\n")
	level.Demo.CmdNewServer(boardWidth, boardHeight, maxMoves, maxFires, maxHealth)

	// Accept connections
	for level.PlayersLeft() < playerCount {
		// If there is connection problem reset the id
		conn, err := ln.Accept()
		if err != nil {
			level.Log("Connection problem: %v\n\n", err)
			continue
		}

		// New connection
		level.Log("New player connected from %v.\n", conn.RemoteAddr())

		// Recieve player data
		buffer := make([]byte, 32)
		n, err := conn.Read(buffer)
		if err != nil {
			level.Log("Connection problem.\n")
			level.Log("Closing connection to %v ...\n\n", conn.RemoteAddr())
			conn.Close()
			continue
		}

		// Decode player data
		name, versionMajor, versionMinor, erb := DecodePlayerData(buffer[:n])
		if erb {
			level.Log("Player data is incorrect.\n")
			level.Log("Closing connection to %v ...\n\n", conn.RemoteAddr())
			conn.Close()
			continue
		}

		// Fill player data
		id := level.AddPlayer(conn, name, versionMajor, versionMinor, maxMoves, maxFires, maxHealth)

		// Prepare and send game config
		conf := EncodeGameConfig(boardWidth, boardHeight, maxMoves, maxFires, maxHealth, playerCount, timeout)
		if !level.Players[id].Write(conf) {
			level.Log("Data sending to '%v' failed. Player will be removed...\n", level.Players[id].Name)
			level.RemovePlayer(id)
			continue
		}

		level.Log("Player '%v' with ID %v is successfully added to the game.\n\n", name, id)
	}

	// Order player list
	level.SortPlayers()

	// Main loop
	gameCount := 1
	turnCount := 0
	newGame := true
	for gameCount < tournament+1 {
		// Check for online players
		if level.PlayersConnected() < 2 {
			level.Log("Not enough players left to play.\n\n")
			break
		}

		if newGame {
			level.Log("Game number %v is starting...\n\n", gameCount)
			level.Demo.CmdNewGame(gameCount)
			newGame = false
			if gameCount > 1 {
				turnCount = 0
				level.ResetBoard()
			}
			for _, v := range level.Players {
				level.Demo.CmdResetPlayer(v.ID, v.X, v.Y)
			}
		}

		// Start a new game
		for _, id := range level.Keys {
			if p, ok := level.Players[id]; ok && !p.isDead {
				// If only 1 player is left declare winner and quit
				if level.PlayersLeft() < 2 {
					level.Log("Player '%v' with ID %v won the game %v in %v turns!\n\n", p.Name, id, gameCount, turnCount)
					level.Demo.CmdWinner(p.Name, id)
					p.GamesWon += 1
					gameCount += 1
					newGame = true
					break
				}

				// Increase turn
				turnCount++
				level.Demo.CmdNewTurn(turnCount, id)

				// Reset player stats
				p.MovesLeft = maxMoves
				p.FiresLeft = maxFires

				// Start turn
				level.Log("Turn %v starts for player '%v' with ID %v.\n", turnCount, p.Name, id)
				level.Log("There are %v players left in the game.\n", level.PlayersLeft())
				level.Log("Player has %v health left.\n", p.Health)

				// Tell player that its turn is starting
				nt := EncodeNewTurn(p.X, p.Y, p.Health, p.MovesLeft, p.FiresLeft, level.PlayersLeft(), level.GetRadar(id))
				if !p.Write(nt) {
					level.Log("Data sending to '%v' failed. Player will be removed...\n", p.Name)
					level.RemovePlayer(id)
					continue
				}

				// Start turn
				garbage := 0
				t0 := time.Now()
				for endTurn := false; !endTurn; {
					// Check for timeout
					delta := time.Now().Sub(t0)
					if delta > time.Duration(timeout)*time.Second {
						level.Log("Uncruical timeout reached. Ending turn...\n\n")
						break
					}

					// Check garbage limit
					if garbage >= garbageLimit {
						level.Log("Player is sending garbage data. Removing player...\n\n")
						level.RemovePlayer(id)
						break
					}

					// Read the command
					buffer := p.Read()
					if buffer == nil {
						level.Log("[SERVER] Data receiving from '%v' failed. Player will be removed...\n", p.Name)
						level.RemovePlayer(id)
						break
					}

					switch buffer[0] {
					// Move command
					case MoveID:
						d, erb := DecodeMove(buffer)
						if !erb {
							level.Move(id, d)
							if !p.Write(EncodeMove(p.X, p.Y, p.MovesLeft, level.GetRadar(id))) {
								level.Log("Data sending to '%v' failed. Player will be removed...\n", p.Name)
								level.RemovePlayer(id)
								endTurn = true
							}
						} else {
							level.Log("Wrong move command.\n")
							garbage += 1
						}

					// Fire command
					case FireID:
						d, erb := DecodeFire(buffer)
						if !erb {
							level.Fire(id, d)
							if !p.Write(EncodeFire(level.PlayersLeft(), p.FiresLeft, level.GetRadar(id))) {
								level.Log("Data sending to '%v' failed. Player will be removed...\n", p.Name)
								level.RemovePlayer(id)
								endTurn = true
							}
						} else {
							level.Log("Wrong fire command.\n")
							garbage += 1
						}

					// End turn command
					case EndTurnID:
						level.Log("Turn ended by player.\n\n")
						endTurn = true

					// If there is too much unknown command, kick player
					default:
						level.Log("Unknown command.\n")
						garbage += 1
					}
				}
			}
		}
	}

	level.SaveLog()
	level.Demo.Save()
}
